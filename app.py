from aiohttp import web
from pathlib import Path
import jinja2
import aiohttp_jinja2
import video
import os
import uuid 
import argparse

UPLOAD_DIR = os.path.join('static', 'uploads')

async def handle(request):
    if request.method == 'POST':
        reader = await request.multipart()

        # read split size
        field = await reader.next()
        assert field.name == 'split_size'
        split_size = await field.read(decode=True)
        split_size = int(split_size)

        # read high precision option
        field = await reader.next()
        assert field.name == 'high_precision'
        high_precision = await field.read(decode=True)
        high_precision = int(high_precision)

        # read video file
        field = await reader.next()
        assert field.name == 'video_file'
        filename = field.filename
        size = 0
        # new file path: UPLOAD_DIR/{uuid}_{split_size}/{filename}
        new_dir = os.path.join(UPLOAD_DIR, str(uuid.uuid1()) + '_' + str(split_size))
        file_path = os.path.join(new_dir, filename)
        Path(new_dir).mkdir(parents=True, exist_ok=True)
        with open(file_path, 'wb') as f:
            while True:
                # read by chunks to minimize RAM usage
                # 8192 bytes by default (increase for faster process)
                chunk = await field.read_chunk() 
                if not chunk:
                    break
                size += len(chunk)
                f.write(chunk)        
        # split video using ffmpeg
        if high_precision:
            result_files = video.split_cut(file_path, split_size, by='size')            
        else:
            result_files = video.split_segment(file_path, split_size, by='size')            
        body = {
            'split_size': split_size,
            'result_files': result_files,
        }
        return web.json_response(body)
    else:
        context = {}
        return aiohttp_jinja2.render_template("index.html", request, context=context)

@web.middleware
async def error_middleware(request, handler):
    try:
        response = await handler(request)
        return response
    except Exception as e:
        print('ERROR: {}'.format(e))
        return web.Response(text=str(e), status=500)

app = web.Application(middlewares=[error_middleware])
aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))
app.add_routes([
    web.static('/static', 'static'),
    web.get('/', handle),
    web.post('/', handle),
])

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Video Splitter Web Server")
    parser.add_argument('--path', default=None)
    parser.add_argument('--port', default=8080)    
    args = parser.parse_args()
    web.run_app(app, path=args.path, port=args.port)