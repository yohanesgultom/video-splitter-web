#!/bin/bash
# Delete uploads leaving the $maxfile most recent files

dirname="static/uploads"
maxfile=$1

printf "maxfile=$maxfile\n"
maxfile=$((maxfile+1))
purging=$(ls -dt "$dirname"/* | tail -n +"$maxfile");
if [ "$purging" != "" ]; then
    printf "Purging old directories:\n$purging\n";
    rm -rf $purging;
else
    printf "No directory found for purging at this time\n";
fi