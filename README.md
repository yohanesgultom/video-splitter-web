# Video Splitter Web

Simple video splitter web

## Development

Dependencies:

* python >= 3.6
* [ffmpeg](https://www.ffmpeg.org/) >= 3.4.6 
* Linux OS (not tested on Windows/Mac)

Running:

1. Install dependencies from project's root: `pip install -r requirements.txt`
1. Run server: `python app.py`
1. Access app from browser: `http://localhost:8080`

## Production

Dependencies:

* python >= 3.6
* [ffmpeg](https://www.ffmpeg.org/) >= 3.4.6 
* Ubuntu server 18.04 LTS
* [supervisor](http://supervisord.org/) 
* [nginx](https://nginx.org/)

Running:

1. Install [virtualenv](https://virtualenv.pypa.io/en/latest/)
2. Create virtual environment in project's root: `virtualenv venv -p python3`
3. Activate virtual environment: `source venv/bin/activate`
4. Install dependencies from project's root: `pip install -r requirements.txt`
5. Deactivate virtual environment: `deactivate`
6. Give `www-data` access to `static/uploads`: `sudo chown -R $USER:www-data static/uploads` 
7. Modify and use `video-splitter-web-worker.conf` to run app using [supervisor](https://docs.aiohttp.org/en/stable/deployment.html#supervisord) (supporting autorestart and multiple workers)
8. Modify and use `vide-splitter-web-nginx` to use nginx together with supervisor 
9. Setup crontab to clean uploads: `0 * * * * cd /home/ubuntu/video-splitter-web && ./clean_uploads.sh 30 > clean_uploads.out` (replace `30` with any number of latest directories you want to keep)
