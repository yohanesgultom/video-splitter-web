function overlay(show) {
    document.getElementById("overlay").style.display = show ? 'block' : 'none'
}

let form = document.querySelector('#upload-video-form')
form.addEventListener("submit", function(e) {
    const tStart = performance.now()
    e.preventDefault()
    overlay(true)
    let result = document.getElementById('results')
    if (result) result.remove()
    const formData = new FormData()
    formData.append('split_size', document.getElementById('split-size').value)
    formData.append('high_precision', document.getElementById('high-precision').checked ? 1 : 0)
    formData.append('video_file', document.getElementById('video-file').files[0])
    fetch('/', {
            method: 'post',
            body: formData,
        })
        .then(function(response) {
            if (!response.ok) {
                return response.text().then((text) => {
                    throw new Error(text)
                })
            } else {
                return response.json()
            }
        })
        .then(function(result) {
            overlay(false)
            let container = document.getElementById('upload-video-form')
            let list = document.createElement('div')
            list.setAttribute('id', 'results')
            list.setAttribute('href', 'results')
            list.className = 'list-group mt-3'
            let title = document.createElement('h3')
            title.className = 'mt-3'
            title.innerHTML = 'Results'
            list.append(title)
            for (let i = 0; i < result.result_files.length; i++) {
                let item = document.createElement('a')
                let path = result.result_files[i]
                let pathArray = path.split('/')
                let name = pathArray[pathArray.length - 1]
                item.setAttribute('href', path)
                item.setAttribute('download', name)
                item.className = 'list-group-item list-group-item-action'
                item.innerHTML = `Part ${i+1}`
                list.appendChild(item)
            }
            container.appendChild(list)
            let tTotal = (performance.now() - tStart) / 1000
            alert(`Process completed in ${tTotal.toFixed(2)} s`)
        })
        .catch(function(error) {
            overlay(false)
            alert(error)
            console.log('Request failed', error)
        })
    return false
})